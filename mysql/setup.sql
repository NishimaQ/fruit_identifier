--
-- Database: `fruit_identifier`
--

-- --------------------------------------------------------

--
-- Table structure for table `fruits`
--

CREATE TABLE `fruits` (
  `fruit_id` int(11) NOT NULL,
  `fruit_name` text NOT NULL,
  `fruit_latin` text NOT NULL,
  `fruit_nutrition` text NOT NULL
) ENGINE = INNODB;

--
-- Dumping data for table `fruits`
--

INSERT INTO `fruits` (`fruit_id`, `fruit_name`, `fruit_latin`, `fruit_nutrition`) VALUES
(1, 'Orange', 'Citrus sinensis', 'Vitamin C, Source of fibre, Source of potassium, Sodium-free, Fat-free'),
(2, 'Papaya', 'Carica papaya', 'High of Vitamin C, High of Vitamin C, High of potassium, Vitamin K, Fibre, Fat-free'),
(3, 'Pineapple', 'Ananas comosus', 'High of Vitamin C, Low in Sodium, Fat-free'),
(4, 'Watermelon', 'Citrullus lanatus', 'Vitamin C, Sodium-free, Fat-free'),
(5, 'Durian', 'Durio zibethinus', 'High of Vitamin C, High of Vitamin B6, High of manganese, High of potassium, Sodium-free'),
(6, 'Guava', 'Psidium guajava', 'High of Vitamin C, High of folacin, potassium, Sodium-free'),
(7, 'Grape', 'Vitis vinifera', 'Low of sodium, Fat-free'),
(8, 'Lemon', 'Citrus limon', 'High of Vitamin C, Sodium-free, Fat-free'),
(9, 'Apple', 'Malus domestica', 'Vitamin C, High of fibre, Sodium-free'),
(10, 'Banana', 'Musa', 'High of potassium, Vitamin C, fibre, Sodium-free, Fat-free');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fruits`
--
ALTER TABLE `fruits`
  ADD PRIMARY KEY (`fruit_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fruits`
--
ALTER TABLE `fruits`
  MODIFY `fruit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;
