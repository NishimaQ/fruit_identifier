const express = require('express')
var mysql = require('mysql')
    // var execsql = require('execsql')
var readline = require('readline');
const fs = require('fs')
const multer = require('multer')
const bodyParser = require('body-parser')
const tf = require('@tensorflow/tfjs-node')
const mobilenet = require('@tensorflow-models/mobilenet')
const toUint8Array = require('base64-to-uint8array')
let model;

const app = express()
app.use(multer({ dest: '/tmp' }).any())
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }))
app.use(bodyParser.json({ limit: '50mb' }))

var db = {
    host: "127.0.0.1",
    database: "fruit_identifier",
    user: "prakppl",
    password: "prakppl",
    port: 3306,
    multipleStatements: true
}

var myCon = mysql.createConnection(db)

myCon.query(`CREATE TABLE fruits (
    fruit_id int(11) NOT NULL,
    fruit_name text NOT NULL,
    fruit_latin text NOT NULL,
    fruit_nutrition text NOT NULL
  ) ENGINE = INNODB;
  
    INSERT INTO fruits (fruit_id, fruit_name, fruit_latin, fruit_nutrition) VALUES
    (1, 'Orange', 'Citrus sinensis', 'Vitamin C, Source of fibre, Source of potassium, Sodium-free, Fat-free'),
    (2, 'Papaya', 'Carica papaya', 'High of Vitamin C, High of Vitamin C, High of potassium, Vitamin K, Fibre, Fat-free'),
    (3, 'Pineapple', 'Ananas comosus', 'High of Vitamin C, Low in Sodium, Fat-free'),
    (4, 'Watermelon', 'Citrullus lanatus', 'Vitamin C, Sodium-free, Fat-free'),
    (5, 'Durian', 'Durio zibethinus', 'High of Vitamin C, High of Vitamin B6, High of manganese, High of potassium, Sodium-free'),
    (6, 'Guava', 'Psidium guajava', 'High of Vitamin C, High of folacin, potassium, Sodium-free'),
    (7, 'Grape', 'Vitis vinifera', 'Low of sodium, Fat-free'),
    (8, 'Lemon', 'Citrus limon', 'High of Vitamin C, Sodium-free, Fat-free'),
    (9, 'Apple', 'Malus domestica', 'Vitamin C, High of fibre, Sodium-free'),
    (10, 'Banana', 'Musa', 'High of potassium, Vitamin C, fibre, Sodium-free, Fat-free');

    ALTER TABLE fruits ADD PRIMARY KEY (fruit_id);

    ALTER TABLE fruits MODIFY fruit_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
    COMMIT;`, function(err, results) {
    if (err) throw err;

    // `results` is an array with one element for every statement in the query:
    console.log(results[0]);
    console.log(results[1]);
    console.log(results[2]);
    console.log(results[3]);
});

// var sql = 'use fruit_identifier;'
// var sqlFile = __dirname + '/setup.sql';

// execsql.config(db)
//     .exec(sql)
//     .execFile(sqlFile, function(err, results) {
//         console.log(results);
//     }).end();

app.post('/predict', async(req, res) => {
    try {
        var suggestedFruit = []

        const imageData = fs.readFileSync(req.files[0].path)
            .toString('base64')
            .replace('data:image/jpeg;base64', '')
            .replace('data:image/png;base64', '')

        const imageArray = toUint8Array(imageData);
        const tensor3d = tf.node.decodeJpeg(imageArray, 3)
        const prediction = await model.classify(tensor3d)
        tensor3d.dispose()

        prediction.forEach(element => {
            suggestedFruit.push(element.className)
        });

        var con = mysql.createConnection(db)
        con.connect(function(err) {
            if (err) throw err;

            var length = suggestedFruit.length,
                i = 0;
            console.log(suggestedFruit)
            for (let fruit of suggestedFruit) {
                var sql = "SELECT * FROM fruits WHERE lower(fruit_name) LIKE '%" + fruit + "%'"
                con.query(sql, function(err, result) {
                    if (err) throw err;
                    if (typeof result[0] !== "undefined") {
                        var response = {
                            'success': true,
                            'message': "Picture has been recognized successfully",
                            'data': {
                                'suggested': suggestedFruit,
                                'selected': JSON.parse(JSON.stringify(result[0]))
                            }
                        }
                        res.statusCode = 200
                        res.send(response)
                        con.end()
                    } else {
                        i++
                        if (i >= length && typeof response == "undefined") {
                            var response = {
                                'success': false,
                                'message': "Picture cannot be recognized",
                                'data': {
                                    'suggested': [],
                                    'selected': {}
                                }
                            }
                            res.statusCode = 400
                            res.send(response)
                            con.end()
                        }
                    }
                })
            }
        })
    } catch (err) {
        var response = {
            'success': false,
            'message': err.message
        }
        res.send(response)
    }
})

app.post('/predict/retry', (req, res) => {
    var fruit = req.body.fruit_name
    try {
        var con = mysql.createConnection(db)
        con.connect((err) => {
            if (err) throw err;

            var sql = "SELECT * FROM fruits WHERE lower(fruit_name) LIKE '%" + fruit + "%'"

            con.query(sql, function(err, result) {
                if (err) throw err

                var response = {
                    'success': true,
                    'message': "Data for suggestion have been gotten",
                    'data': {
                        'selected': JSON.parse(JSON.stringify(result[0]))
                    }
                }
                res.send(response)
                con.end()
            })
        })
    } catch (err) {
        var response = {
            'success': false,
            'message': err.message
        }
        res.send(response)
    }
})

app.get('*', (req, res) => {
    var response = {
        'success': false,
        'message': "Not found"
    }
    res.send(response)
})

app.listen(8080, async() => {
    console.log(`Loading imagenet model`);
    model = await mobilenet.load({
        version: 1,
        alpha: 0.25 | .50 | .75 | 1.0,
    });
    console.log(`Imagenet model loaded`);
    console.log(`Server listening on port ${8080}!`)
})